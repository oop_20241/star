import java.util.Scanner;

public class problem4 {
    private static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        double  sum = 0;
        int count = 0;

        while(true){
            System.out.print("Please input number: ");
            int num = input.nextInt();

            if(num == 0){
                System.out.println("Bye");
                break;
            }

            sum+=num;
            count++; 
            double avg = sum/count;

            System.out.println("Sum: " + sum + " Avg: " + avg);
     
        }
        input.close();

    }
}
