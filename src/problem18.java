import java.util.Scanner;

public class problem18 {
    private static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        while(true){
            System.out.print("Please select star type[1-4,5 is Exit]: ");
            int select = input.nextInt();
            if (select > 0 && select < 5){
                System.out.print("Please input number: ");
                if(select == 1){
                    int num1 = input.nextInt();
                    for(int i = 0 ; i < num1 ; i++ ){
                        for(int j = 0 ; j<=i ; j++){
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                }
                else if( select == 2){
                    int num2 = input.nextInt();
                    for(int i = 0 ; i < num2 ; i++){
                        for(int j = num2 ; j>i ; j--){
                            System.out.print("*");
                        }
                        System.out.println();
                    }

                }
                else if(select == 3){
                    int num3 = input.nextInt();
                    for(int i = 0 ; i < num3 ; i++){
                        for(int j = 0 ; j<i ; j++){
                            System.out.print(" ");
                        }
                        for (int j = num3 ; j>i ; j--){
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                }
                else if(select == 4){
                    int num4 = input.nextInt();
                    for(int i = 0 ; i < num4 ; i++){
                        for(int j = num4-1 ; j>i ; j--){
                            System.out.print(" ");
                        }
                        for (int j = 0 ; j<=i ; j++){
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                }
            }
            else if(select == 5){
                System.out.println("Bye bye!!!");
                break;
            }
            else {
                System.out.println("Error: Please input number between 1-5");
            }
        }        
    }
}
