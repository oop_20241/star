public class problem8 {
    public static void main(String[] args) {
        int num = 5;
        for(int i = 0 ; i < num ; i++){
            for (int j = 1 ; j<=num ; j++ ){
                System.out.print(j);
            }
            System.out.println();
        }

        System.out.println();

        int i = 0 ;
        while (i < num){
            int j = 1 ;
            while(j <= num){
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}
