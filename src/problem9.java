import java.util.Scanner;

public class problem9 {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print("Please input n: ");
        int num = input.nextInt();
        for(int i = 0 ; i < num ; i++){
            for (int j = 1 ; j<=num ; j++ ){
                System.out.print(j);
            }
            System.out.println();
        }

        System.out.println();

        int i = 0 ;
        while (i < num){
            int j = 1 ;
            while(j <= num){
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}

